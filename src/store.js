import { action, thunk } from "easy-peasy";

export const model = {
	users: [],
	posts: [],
	photos: [],
	fetchUsers: thunk(async (actions) => {
		const res = await fetch("https://jsonplaceholder.typicode.com/users");
		const users = await res.json();
		await actions.setUsers(users);
	}),
	fetchPosts: thunk(async (actions) => {
		const res = await fetch("https://jsonplaceholder.typicode.com/posts");
		const posts = await res.json();
		await actions.setPosts(posts);
	}),
	fetchPhotos: thunk(async (actions) => {
		const res = await fetch("https://picsum.photos/v2/list");
		const photos = await res.json();
		await actions.setPhotos(photos);
	}),
	setUsers: action((state, payload) => {
		return {
			...state,
			users: [...payload]
		};
	}),
	setPosts: action((state, payload) => {
		return {
			...state,
			posts: [...payload]
		};
	}),
	setPhotos: action((state, payload) => {
		return {
			...state,
			photos: [...payload]
		};
	}),
	creatPostThunk: thunk(async (actions, payload) => {
		const res = await fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			body: JSON.stringify({
			  title: payload.title,
			  body: payload.body,
			  userId: 1,
			}),
			headers: {
			  'Content-type': 'application/json; charset=UTF-8',
			},
		  });
		const newPost = await res.json();
		await actions.createPost(newPost);
	}),
	createPost: action((state, payload) => {
		return {
			...state,
			posts: [...state.posts, payload] //BUG: mass the post order and edit dialog
		};
	}),
	updatePostThunk: thunk(async (actions, payload) => {
		const requestUrl = 'https://jsonplaceholder.typicode.com/posts/' + payload.id; 
		const res = await fetch(requestUrl, {
			method: 'PUT',
			body: JSON.stringify({
				id: payload.id,
			  	title: payload.title,
			  	body: payload.body,
			  	userId: payload.userId,
			}),
			headers: {
			  'Content-type': 'application/json; charset=UTF-8',
			},
		  });
		const currentPost = await res.json();
		await actions.updatePost(currentPost);
	}),
	updatePost: action((state, payload) => {
		return {
			...state,
			posts:  state.posts.map((p) =>
			p.id === payload.id ? { ...p, ...payload } : { ...p }
		)
		};
	}),
	deletePostThunk: thunk(async (actions, payload) => {
		const url = 'https://jsonplaceholder.typicode.com/posts/' + payload.id;
		const res = await fetch(url, {
			method: 'DELETE'});
		await actions.deletePost(payload);
	}),
	deletePost: action((state, payload) => {
		let finalList = [];
		state.posts.map((p) =>
			p.id !== payload.id ? finalList.push(p) : ""
		);
		return {
			...state,
			posts: finalList
		};
	}),
	userSession: {
		isLoggedIn: false,
		user: null
	}
};
