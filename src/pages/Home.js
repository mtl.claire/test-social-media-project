import React from "react";
import { useEffect } from "react";
import { useStoreState, useStoreActions } from "easy-peasy";
import { PostCardItem } from "../components/Card";
import { makeStyles } from "@material-ui/core/styles";
import { AddPostDialog } from "./Add";

const useStyles = makeStyles((theme) => ({
	root: {
		margin: "0"
    },
    wrapper: {
        padding: "10px"
    }
}));

export const HomeContainer = () => {
	const classes = useStyles();
    const fetchPosts = useStoreActions((actions) => actions.fetchPosts);
    const posts = useStoreState((state) => state.posts);
    const renderedListItems = posts.map((item, index) => {
        return <PostCardItem key={ index } index={ index } item={ item }/>;
    });
    useEffect(() => {
        fetchPosts();
    }, []);
    return ( 
        <div className={classes.root}>
            <ul className={classes.wrapper}> { renderedListItems } </ul> 
            <AddPostDialog />
        </div>
    );
};
