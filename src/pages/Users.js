import React from "react";
import { useEffect } from "react";
import { Badge, Avatar, Divider } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { useStoreState, useStoreActions } from "easy-peasy";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import PhoneEnabledIcon from '@material-ui/icons/PhoneEnabled';

export const UsersContainer = () => {
    const fetchUsers = useStoreActions((actions) => actions.fetchUsers);
	useEffect(() => {
        fetchUsers();
    }, []);
	return (
        <div>
			<h2>Call Friends</h2>
			<UserListComponent />
		</div>
    );
};

const UserListComponent = () => {
    const users = useStoreState((state) => state.users);
    const renderedListItems = users.map((item, index) => {
        return (
            <UserItem key={index} index={index} item={item} />
        )
    });

    return (
        <div id="user-container">
			<List>
				{renderedListItems}
			</List>
		</div>
    );
};

const StyledBadge = withStyles((theme) => ({
    badge: {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            content: '""',
        },
    },
}))(Badge);

const useStyles = makeStyles((theme) => ({
    userItem: {
		padding: "16px",
		borderBottom: "1px solid #f5f2f2"
	},
	avatarItem: {
		backgroundColor: "#3f51b5" 
	}
}));

const UserItem = ({ item }) => {
	const classes = useStyles();
	return (
        <ListItem className={classes.userItem}>
			<ListItemAvatar>
				<StyledBadge
				overlap="circle"
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				variant="dot"
				>
				<Avatar alt={item.name} className={classes.avatarItem} src="/static/images/avatar/1.jpg" />
				</StyledBadge>
			</ListItemAvatar>
			<ListItemText
			primary={item.name}
			secondary={item.phone}
			/>
			<Divider />
			<ListItemSecondaryAction>
				<IconButton edge="end" aria-label="delete">
					<PhoneEnabledIcon />
				</IconButton>
			</ListItemSecondaryAction>
		</ListItem>
    );
}