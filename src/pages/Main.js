import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { Photo as PhotoIcon, Home as HomeIcon, PersonPin as PersonPinIcon } from "@material-ui/icons";
import { HomeContainer } from "./Home";
import { UsersContainer } from "./Users";
import { PhotosContainer } from "./Photos";
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

const useStyles = makeStyles({
    root: {
        height: "100vh",
        margin: "70px 0"
    },
    toolBar: {
        width: "100%",
        position: "fixed",
        bottom: 0,
        left: 0,
        boxShadow: "0px -1px 20px 0px #d8d8d8"
    },
});

export const MainContainer = () => {
    const classes = useStyles();
    return (
        <Router>
            <div className={classes.root}>
              <Switch>
                <Route exact path="/">
                 	<HomeContainer />
                </Route>
                <Route path="/photos">
                 	<PhotosContainer />
                </Route>
                <Route path="/users">
                 	<UsersContainer />
                </Route>
              </Switch>
            </div>
            <BottomNav />
        </Router>
    );
};

const BottomNav = () => {

    const classes = useStyles();
    const [value, setValue] = React.useState('recents');
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
	    <BottomNavigation value={value} onChange={handleChange} className={classes.toolBar}>
	      	<BottomNavigationAction component={Link} to="/" label="Home" value="recents" icon={<HomeIcon />} />
	      	<BottomNavigationAction component={Link} to="/photos" label="Photos" value="favorites" icon={<PhotoIcon />} />
	      	<BottomNavigationAction component={Link} to="/users" label="Friends" value="nearby" icon={<PersonPinIcon />} />
	    </BottomNavigation>
    );
}