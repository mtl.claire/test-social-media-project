import React from "react";
import { useEffect } from "react";
import { useStoreState, useStoreActions } from "easy-peasy";
import ImgMediaCard from "../components/PhotoItem";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "0"
    },
    wrapper: {
        padding: "10px"
    }
}));

export const PhotosContainer = () => {
    const classes = useStyles();
    const fetchPhotos = useStoreActions((actions) => actions.fetchPhotos);
    const photos = useStoreState((state) => state.photos);
    const renderedListItems = photos.map((item, index) => {
        return <ImgMediaCard key={ index } index={ index } item={ item }/>;
    });
    useEffect(() => {
        fetchPhotos();
    }, []);
    return (
        <div className={classes.root}>
            <ul className={classes.wrapper}> { renderedListItems } </ul> 
        </div>
    );
};