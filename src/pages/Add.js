import React from "react";
import { useStoreActions } from "easy-peasy";
import { makeStyles } from "@material-ui/core/styles";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "0"
    },
    fabButton: {
        position: 'fixed',
        zIndex: 1,
        bottom: 70,
        right: 20,
        color: "#fff",
        backgroundColor: "#303f9f"
    },
    title: {
        flexGrow: 1,
        textAlign: "center"
    },
    editContainer: {
        padding: "70px 10px"
    },
    textField: {
        width: "calc(100% - 32px)",
        padding: "0 16px"
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


export const AddPostDialog = ({ item }) => {
    const classes = useStyles();
    const creatPostThunk = useStoreActions((actions) => actions.creatPostThunk);
    const [open, setOpen] = React.useState(false);
    const [valueTitle, setValueTitle] = React.useState("");
    const [valueBody, setValueBody] = React.useState("");

    const handleChangeTitle = (event) => {
        setValueTitle(event.target.value);
    };

    const handleChangePost = (event) => {
        setValueBody(event.target.value);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = (event) => {
        let isSave = event.currentTarget.name === "save";
        if (isSave) {
            let data = {
                body: valueBody,
                title: valueTitle
            };
            creatPostThunk(data);
        }
        setOpen(false);
    };

    return (
        <div>
            <Fab aria-label="addPost" className={classes.fabButton} onClick={handleClickOpen}>
                <AddIcon />
            </Fab>
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                        Add a New Post
                        </Typography>
                    </Toolbar>
                </AppBar>
                <List className={classes.editContainer}>
                    <ListItem>
                        <ListItemText primary="Title" label="Required"/>
                    </ListItem>
                    <TextField
                        className={classes.textField}
                        autoFocus
                        required
                        margin="dense"
                        value={valueTitle}
                        onChange={handleChangeTitle}
                    />
                    <ListItem>
                        <ListItemText primary="Post"/>
                    </ListItem>
                    <TextField
                        className={classes.textField}
                        autoFocus
                        required
                        margin="dense"
                        value={valueBody}
                        multiline
                        onChange={handleChangePost}
                    />
                    <ListItem>
                        <Button autoFocus name="save" variant="outlined" color="primary" onClick={handleClose}>
                        Save
                        </Button>
                    </ListItem>
                </List>
            </Dialog>
        </div>
    );
};