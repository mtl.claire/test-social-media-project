import React from "react";
import { Header } from "./components/Header";
import { MainContainer } from "./pages/Main";

export default function App() {
	return (
		<div className="App">
			<Header />
			<MainContainer />
		</div>
	);
}
