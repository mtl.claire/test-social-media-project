import React from "react";
import ReactDOM from "react-dom";
import { StoreProvider, createStore } from "easy-peasy";
import App from "./App";
import { model } from "./store";

const store = createStore(model);

ReactDOM.render(
	<StoreProvider store={store}>
		<App />
	</StoreProvider>,
	document.getElementById("root")
);
