import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Translate from "@material-ui/icons/Translate";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	menuButton: {
		marginRight: theme.spacing(2)
	},
	title: {
		flexGrow: 1
	}
}));

export const Header = () => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<AppBar position="fixed" color="inherit">
				<Toolbar>
					<Typography variant="h6" className={classes.title}>
						LOGO
					</Typography>
					<FormGroup>
						<FormControlLabel
							control={
								<Switch
									color="primary"
									aria-label="login switch"
								/>
							}
						/>
					</FormGroup>
					<IconButton
						aria-label="account of current user"
						aria-controls="menu-appbar"
						aria-haspopup="true"
						color="inherit">
						<Translate />
					</IconButton>
				</Toolbar>
			</AppBar>
		</div>
	);
};
