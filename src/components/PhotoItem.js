import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
    root: {
        display: "inline-flex",
        width: "50%",
        padding: "5px"
    },
});

export default function ImgMediaCard({item}) {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
        <Grid item xs={12}>
            <Card>
                <CardActionArea>
                    <CardMedia
                    component="img"
                    alt={item.auther+item.url}
                    height="140"
                    image={item.download_url}
                    title="Contemplative Reptile"
                    />
                </CardActionArea>
            </Card>
        </Grid>
    </Grid>
  );
}
