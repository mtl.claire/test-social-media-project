import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import { useStoreActions } from "easy-peasy";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 345,
		margin: "0 0 20px 0"
	},
	media: {
		height: 0,
		paddingTop: "56.25%" // 16:9
	},
	title: {
		flexGrow: 1,
		textAlign: "center"
	  },
	expand: {
		transform: "rotate(0deg)",
		marginLeft: "auto",
		transition: theme.transitions.create("transform", {
			duration: theme.transitions.duration.shortest
		})
	},
	expandOpen: {
		transform: "rotate(180deg)"
	},
	editContainer: {
		padding: "70px 10px"
	},
	textField: {
		width: "calc(100% - 32px)",
		padding: "0 16px"
	},
	avatar: {
		backgroundColor: "#f50057"
	}
}));

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
  });

export const PostCardItem = ({item}) => {
	const classes = useStyles();
	const updatePostThunk = useStoreActions((actions) => actions.updatePostThunk);
	const deletePostThunk = useStoreActions((actions) => actions.deletePostThunk);

	const [expanded, setExpanded] = React.useState(false);
	const handleExpandClick = () => {
		setExpanded(!expanded);
	};
	
	const [valueTitle, setValueTitle] = React.useState(item.title);
	const handleChangeTitle = (event) => {
	  setValueTitle(event.target.value);
	};

	const [valueBody, setValueBody] = React.useState(item.body);
	const handleChangePost = (event) => {
	  setValueBody(event.target.value);
	};

	const [open, setOpen] = React.useState(false);
	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = (event) => {
		console.log(event.currentTarget);
		let isSave = event.currentTarget.name === "save";
		let isDelete = event.currentTarget.name === "delete";

		if(isSave) {
			let data = {
				...item, body: valueBody, title: valueTitle
			}
			updatePostThunk(data);
		}
		if(isDelete) {
			deletePostThunk(item);
		}
		setOpen(false);
	};

	return (
		<Card className={classes.root}>
			<CardHeader
				avatar={
					<Avatar aria-label="recipe" className={classes.avatar}>
						{item.userId}
					</Avatar>
				}
				action={
					<IconButton aria-label="settings"  onClick={handleClickOpen}>
						<MoreVertIcon />
					</IconButton>
				}
				title={item.title}
				subheader="September 14, 2016"
			/>
			<CardMedia
				className={classes.media}
				image="https://cdn.pixabay.com/photo/2018/01/11/09/52/three-3075752_1280.jpg"
				title="Paella dish"
			/>
			<CardContent>
				<Typography variant="body2" component="p">
					{item.body}
				</Typography>
			</CardContent>
			<CardActions disableSpacing>
				<IconButton aria-label="add to favorites">
					<FavoriteIcon />
				</IconButton>
				<IconButton
					className={clsx(classes.expand, {
						[classes.expandOpen]: expanded
					})}
					onClick={handleExpandClick}
					aria-expanded={expanded}
					aria-label="show more">
					<ExpandMoreIcon />
				</IconButton>
			</CardActions>
			<Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
				<AppBar className={classes.appBar}>
					<Toolbar>
						<IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
						<CloseIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
						Edit Post
						</Typography>
						<Button autoFocus name="delete" variant="contained" color="secondary" onClick={handleClose}>
							Delete
						</Button>
					</Toolbar>
				</AppBar>
				<List className={classes.editContainer}>
					<ListItem>
						<ListItemText primary="Title" />
					</ListItem>
					<TextField
						className={classes.textField}
						autoFocus
						margin="dense"
						value={valueTitle}
						onChange={handleChangeTitle}
					/>
					<ListItem>
						<ListItemText primary="Post"/>
					</ListItem>
					<TextField
						className={classes.textField}
						autoFocus
						margin="dense"
						value={valueBody}
						multiline
						onChange={handleChangePost}
					/>
					<ListItem>
						<Button autoFocus variant="outlined" color="primary" name="save" onClick={handleClose}>
						Save
						</Button>
					</ListItem>
				</List>
			</Dialog>
			<Collapse in={expanded} timeout="auto" unmountOnExit>
				<CardContent>
					<Typography paragraph>Method:</Typography>
					<Typography paragraph>
						Heat 1/2 cup of the broth in a pot until simmering, add saffron and
						set aside for 10 minutes.
					</Typography>
					<Typography paragraph>
						Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet
						over medium-high heat. Add chicken, shrimp and chorizo, and cook,
						stirring occasionally until lightly browned, 6 to 8 minutes.
						Transfer shrimp to a large plate and set aside, leaving chicken and
						chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes,
						onion, salt and pepper, and cook, stirring often until thickened and
						fragrant, about 10 minutes. Add saffron broth and remaining 4 1/2
						cups chicken broth; bring to a boil.
					</Typography>
					<Typography paragraph>
						Add rice and stir very gently to distribute. Top with artichokes and
						peppers, and cook without stirring, until most of the liquid is
						absorbed, 15 to 18 minutes. Reduce heat to medium-low, add reserved
						shrimp and mussels, tucking them down into the rice, and cook again
						without stirring, until mussels have opened and rice is just tender,
						5 to 7 minutes more. (Discard any mussels that don’t open.)
					</Typography>
					<Typography>
						Set aside off of the heat to let rest for 10 minutes, and then
						serve.
					</Typography>
				</CardContent>
			</Collapse>
		</Card>
	);
};
